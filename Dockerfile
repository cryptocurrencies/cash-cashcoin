# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/CryptoNatriX/cash.git /opt/cash && \
    cd /opt/cash/src && \
    make -j2 -f makefile.unix

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
     apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r cash && useradd -r -m -g cash cash
RUN mkdir /data
RUN chown cash:cash /data
COPY --from=build /opt/cash/src/Cashd /usr/local/bin/
USER cash
VOLUME /data
EXPOSE 3352 3491
CMD ["/usr/local/bin/Cashd", "-datadir=/data", "-conf=/data/Cash.conf", "-server", "-txindex", "-printtoconsole"]